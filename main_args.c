#include<stdio.h>

/* La c'est la maniere "officielle" de declarer les arguments du main.
 * La maniere plus "simple" que tu vas faire au debut c'est :
 * 
 * int main(int argc, char **argv)
 */
int main(int argc, char const *argv[])
{
    printf("Il y a %i argument(s)\n", argc);

    for(int i = 0; i<argc; i++) {
        printf("argument %i : %s\n", i, argv[i]);
    }
    return 0;
}
