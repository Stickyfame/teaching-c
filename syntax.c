#include<stdio.h>

/*
 * type_de_retour nom_de_fonction(arguments) {
 *      code
 *      return 
 * }
 */
int add(int a, int b) {
    return a+b;
}

/*
 * exemple de fonction qui ne retourne rien, et de comment
 * on commente une fonction
 */

/** 
 * @brief affiche un entier dans le terminal
 * 
 * @param a l'entier que l'on veut afficher
 */
void print_int(int a) {
    printf("%i\n", a);
    // pas de return car le type de la fonction est 'void'
}

int main()
{
    /* declarer des variables
     * type nom_de_variable;
     * 
     * assigner une valeur a une variable
     * nom_de_variable = valeur;
     */
    int res;
    float poro;

    poro = 2.5;

    /* appel a une fonction. Le type de retour de la fonction doit correspondre
     * au type de la variable dans laquelle on stocke le retour de la fonction
     */
    res = add(2, 3);

    /* appel a une fonction qui ne retourne rien */
    print_int(res);    
    return 0;
}
