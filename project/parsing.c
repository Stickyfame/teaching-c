#include "parsing.h"

void parse_args(char ** argv, char *** winners, char *** loosers, int ** winnersMMR, int ** loosersMMR)
{
    int i;
    int winCrsr;
    int losCrsr;
    int winMmrCrsr;
    int losMmrCrsr;

    winCrsr = 0;
    losCrsr = 0;
    winMmrCrsr = 0;
    losMmrCrsr = 0;

    /* initialize array for the 5 winners name */
    *winners = (char **) malloc(5 * sizeof(char *));
    /* initialize array for the 5 loosers name */
    *loosers = (char **) malloc(5 * sizeof(char *));

    /* initialize MMR arrays */
    *winnersMMR = (int *) malloc(5 * sizeof(int));
    *loosersMMR = (int *) malloc(5 * sizeof(int));

    /* i starts at 2 because it represents the indice in the argv array.
       argv[0] is the executable's name, argv[1] is the win information,
       so the players information start at argv[2], hence i=2 */
    i = 1;
    /* there is 20 arguments in argv, so i < 21 */
    while (i < 21)
    {
        /* if i is odd = we are reading a player name */
        if (i % 2 == 1)
        {
            /* we are reading winners */
            if (i < 11)
            {
                (*winners)[winCrsr] = argv[i];
                winCrsr++;
            }
            /* we are reading loosers */
            else
            {
                (*loosers)[losCrsr] = argv[i];
                losCrsr++;
            }
        }

        /* if i is even = we are reading a player MMR */
        else
        {
            if (i < 11)
            {
                (*winnersMMR)[winMmrCrsr] = atoi(argv[i]);
                winMmrCrsr++;
            }
            else
            {
                (*loosersMMR)[losMmrCrsr] = atoi(argv[i]);
                losMmrCrsr++;
            }            
        }

        i++;
    }
    
}