# LP calculator

## Inputs

- name of 10 players
- MMR of 10 players
- we assume that the first 5 players are the winners

## Outputs

- the LP loss / gain for every player

## Example

```sh
./a.out player1 1265 player2 1131 player3 3483 player4 3461 ... player10 1850

player1: +21
player2: +23
player3: +10
...
player9: -15
player10: -12
```

## Additional rules

- use of `stdio.h` as long as `printf` is forbidden
- use of `for` loop is forbidden
- declaration of arrays with the syntax `int[] array` is forbidden. Only use `int * array` with `malloc`. Access to an array can be made with the syntax `array[i]`
- only external libs allowed are `unistd.h` and `stdlib.h`
- your functions must not exceed 50 lines
- your program **must** have at least 3 `.c` files and 2 `.h` files
- your program must return an error if there is not exactly 21 arguments passed to the executable
- your program must compile without any error or warning with the following flags : `gcc -Wall -Wextra -Werror ...`
- you must regularly commit and push your code to this repository, with *useful* commits messages

## Tips

- implement a `print_str(char * str)` function with the help of the `write` function
- implement a `print_int(int a)` function with the help of the `write` function
- divide your code in multiples files : one file for print-related stuff, one file for computation-related stuff ...
- use the `atoi` function to convert a string to an integer (for example to convert "123" into the integer 123)
- implement a function to convert an integer to a string (useful if you want to print an integer and you already have the `print_str` function)
- don't forget to call `man` if you don't know something (`man atoi`)
