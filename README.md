# Apprendre les bases de la programmation

## 1. Configurer l'environnement

Pour commencer, il vous faudra un environnement Unix. Pourquoi ? Parce que la TRES grande majorite des applications, codes, logiciels, etc. actuels sont developpes sous un OS Unix. Cet OS permet une tres grande flexibilite et configuration appreciable lorsque l'on developpe du code. En plus, Linux (un des principaux OS Unix like) c'est Open Source et gratuit.

### Set Up sur Windows

- control panel / programmes / activer fonctionnalite windows / cocher sous-systeme linux
- reboot
- microsoft store / installer Ubuntu 20.04
- lancer Ubuntu / configurer user et password
- microsoft store / installer Windows terminal
- lancer Windows terminal / aller dans les parametres en json
- dans "default" en haut, mettre le guid du profil "Ubuntu"
- dans le profil Ubuntu, ajouter "startingDirectory": "//wsl$/Ubuntu-20.04/home/{user}"
- c'est bon !

### Set Up sur un OS Unix (Mac OS / Linux)

- ouvrir le terminal
- c'est bon !

## 2. Les bases du shell / terminal

Ici, pas d'interface graphique. On fait tout a partir de commandes dans le terminal. Pourquoi ? Car la majorite des serveurs du monde entier sont en fait des systemes d'exploitation Linux qui tournent sans interface graphique (c'est inutile, cela consomme des ressources alors que personne ne va se logger dessus comme sur un ordi perso), et a peu pres n'importe quelle application web aujourd'hui fonctionne sur un serveur. Autant dire qu'il y en a beaucoup.

Vient donc le jour ou il faudra configurer un serveur pour qu'il fasse tourner votre application. Vous n'aurez alors pas acces a une interface graphique, vous serez donc bien content de savoir vous servir d'un terminal.

> Il est bon de noter que les premiers ordinateurs ne fonctionnait qu'en mode terminal.

Dans un terminal, on parcours la hirarchie de fichiers du systeme. En Unix, la racine du systeme de fichiers est le repertoire `/`, il s'appelle `root` (oui racine quoi). Il faut se representer le systeme de fichier comme une arborescence a partir de la racine. Ainsi, on peut representer chaque fichier du systeme comme suit :

```bash
/repertoire1/repertoire2/fichier.txt
```

Dans le terminal, on est toujours dans un repertoire particulier et on va executer des commandes a partir de ce repertoire dans la hierarchie de fichiers. C'est un peu comme l'explorateur de fichier Windows quoi. Le repertoire courant s'appelle `working directory`.

### 2.1. La ligne de commande (ou CLI pour Command Line Interface)

Les systemes Unix fournissent un ensemble de "commandes" de base qui permetten de naviguer dans l'arborescence de fichiers de l'ordi. Une commande est simplement une suite de mots que l'on tape dans le terminal, puis on l'execute et on lit ce que retourne la commande sur le terminal. Une commande est en fait un **executable** (Au sens un programme qui s'execute) qui n'a pas d'interface graphique. Votre naviguateur est un executable, vous pouvez d'ailleurs lancer firefox via votre terminal. Tapez simplement `firefox` dans le terminal (bon pas si vous etes sous windows avec le WSL).

Les commandes se presentent sous la forme :

```sh
nom-de-la-commande [option(s)] <argument(s)>
```

Les `[ ]` signifient que les options sont **optionnelles** (nan jure). Les `< >` signifient que les arguments sont **requis**. Certains arguments peuvent etre optionnels. Voici un exemple de commande avec des options et des arguments :

```bash
ls -l developpement/
```

- La commande est `ls`, elle sert a lister les fichiers d'un repertoire
- L'option est `-l`, elle sert a afficher le resultat sous forme de liste a points
- L'argument est `developpement/`, cela signifie que l'on veut afficher le contenu de ce repertoire

Les commandes les plus utilisees sont :

- `ls` pour lister le contenu d'un repertoire
- `cd` pour changer de repertoire (Change Directory)
- `cp` pour copier
- `mv` pour deplacer / renommer
- `rm` pour supprimer
- `mkdir` pour creer un repertoire
- `touch` pour creer un fichier (plus ou moins)
- `pwd` pour afficher le repertoire courant (Print Working Directory)

Pour savoir ce que fait une commande, votre meilleur ami : le **manuel**. Pour le lire rien de plus simple : `man <command>`. Par exemple, pour connaitre les differentes options de la commande `ls`, taper `man ls`.

> Pour quitter la page de manuel, taper sur `Q`. Pour rechercher un mot, tapez `/` puis le mot en question. Parcourez les occurences du mot avec `n` et `MAJ+n`.
